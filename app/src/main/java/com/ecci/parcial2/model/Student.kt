package com.ecci.parcial2.model

data class Student(var name: String, var lastName: String, var doc: String, var phone: String, var isSelected: Boolean)

