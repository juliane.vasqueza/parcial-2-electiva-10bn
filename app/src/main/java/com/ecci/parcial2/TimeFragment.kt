package com.ecci.parcial2

import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_time.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class TimeFragment : Fragment() {


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_time, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateTimeButton.setOnClickListener {
            showTimePickerDialog()
        }
    }
        private fun showTimePickerDialog() {
            val calendar = Calendar.getInstance()
            val hour = calendar.get(Calendar.HOUR_OF_DAY)
            val minute = calendar.get(Calendar.MINUTE)
            val dialog = TimePickerDialog(requireContext(),

                    {  _, hour, minute ->
                      var hourAux =hour.toString()
                        var minuteAux =minute.toString()
                        if (hour.toString().length==1){ hourAux = "0".plus(hourAux) }
                        if (minute.toString().length==1){ minuteAux = "0".plus(minuteAux) }

                textViewTime.setText("Hora: " + hourAux + ":" + minuteAux )
                        Toast.makeText(requireContext(),
                                "Seleccionó la hora ".plus(hourAux).plus(":").plus(minuteAux), Toast.LENGTH_SHORT).show()
            }, hour, minute, true)
            dialog.setCancelable(false)
            dialog.setTitle("Seleccione la hora")
            dialog.setMessage("Selecciona la hora usando este diálogo")
            dialog.show()

        }
    }
