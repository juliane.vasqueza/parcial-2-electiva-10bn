package com.ecci.parcial2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        buttonTime.setOnClickListener {
            showTimeFragment()
        }
        buttonImage.setOnClickListener {
            showEditImageFragment()
        }
    }

    private fun showTimeFragment() {
        val  transaction=supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, TimeFragment(), null)
        transaction.commit()
    }
    private fun showEditImageFragment() {
        val  transaction=supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, EditImageFragment(), null)
        transaction.commit()
    }
}
