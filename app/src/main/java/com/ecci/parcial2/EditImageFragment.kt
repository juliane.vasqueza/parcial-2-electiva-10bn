package com.ecci.parcial2

import android.app.AlertDialog
import android.content.res.Resources
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import com.ecci.parcial2.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_edit_image.*
import kotlinx.android.synthetic.main.fragment_time.*


class EditImageFragment : Fragment() {

var imageChanged = true

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_image, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageButtonEdit.setOnClickListener {
            imageChanged=!imageChanged
            changeImage()
        }
    }
    private fun changeImage() {

        if (imageChanged){
            imageView.setImageResource(R.drawable.ic_image_dog)
        }else {
            imageView.setImageResource(R.drawable.ic_image_wolf)
        }
    }
}


