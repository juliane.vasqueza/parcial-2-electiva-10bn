package com.ecci.parcial2.adapter

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.ecci.parcial2.R
import com.ecci.parcial2.model.Student
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter(val students: MutableList<Student>, var delegate: StudentProtocol, val callback: (Student, Boolean) -> Unit): RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    class StudentViewHolder(itemView: View, var delegate: StudentProtocol, val callback: (Student, Boolean) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private lateinit var adapter: StudentAdapter

        @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables")
        fun bind(item: Student, students:  MutableList<Student>) {
            delegate.hideActivtyText(students)
            itemView.nameTextView.text = item.name
            itemView.lastNameTextView.text = item.lastName
            itemView.docStudentTextView.text = item.doc
            itemView.phoneStudentTextView.text = item.phone
            itemView.deleteButton.setOnClickListener {
                val builder = AlertDialog.Builder(itemView.context)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Esta seguro que quiere eliminar el estudiante?")
                builder.setPositiveButton("Si"){_,_ ->
                    callback(item, true)
                }
                builder.setNegativeButton("Cancelar"){_,_ ->
                }
                builder.show()
            }

            itemView.editButton.setOnClickListener {
                callback(item, false)
            }


            itemView.setOnClickListener {
                item.isSelected = !item.isSelected

                delegate.didProductSelect(item)

            }

            if(item.isSelected){
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.DarkRed))
                delegate.productsToDelete(item, true)
            }else{
                itemView.setBackgroundColor(itemView.resources.getColor(R.color.darkBlue))
                delegate.productsToDelete(item, false)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
        return StudentViewHolder(view,  delegate,callback)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(students[position], students)
    }

    override fun getItemCount(): Int {
        return students.size
    }

    fun addStudent(student: Student) {
        students.add(student)
    }

    fun deleteProduct(student: Student) {
        students.remove(student)
    }



    fun editStudent(student: Student, newName: String, newLastName: String, newDoc: String, newPhone: String) {
        val index = students.indexOf(student)
        students[index].name = newName
        students[index].lastName = newLastName
        students[index].doc = newDoc
        students[index].phone = newPhone
    }

}

interface StudentProtocol {
    fun didProductSelect(pruduct: Student)
    fun productsToDelete(pruduct: Student, isToDelete: Boolean)
    fun hideActivtyText(students: MutableList<Student>)
}