package com.ecci.parcial2.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.ecci.parcial2.R
import kotlinx.android.synthetic.main.dialog_student.*

class StudentDialog(context: Context, val name: String, val lastName: String, val doc: String, val phone: String, val isModifier: Boolean, private val callback: (String, String, String, String) -> Unit) : Dialog(context) {
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_student)
        nameEditText.setText(name)
        lastNameEditText.setText(lastName)
        docEditText.setText(doc)
        phoneEditText.setText(phone)

        saveButton.setOnClickListener {
            //Validación para que todos los campos sean diligenciados
            if (nameEditText.text.isEmpty() || lastNameEditText.text.isEmpty() || docEditText.text.toString().isEmpty() || phoneEditText.text.toString().isEmpty()){
                dialogError.setText("Todos los campos son obligatorios")
                dialogError.visibility = View.VISIBLE
            }
            else{
                val name = nameEditText.text.toString()
                val lastName = lastNameEditText.text.toString()
                val doc = docEditText.text.toString()
                val phone = phoneEditText.text.toString()
                callback(name, lastName, doc, phone)
                dismiss()
            }
        }
    }
}