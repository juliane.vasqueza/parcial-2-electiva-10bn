package com.ecci.parcial2


import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.parcial2.adapter.StudentAdapter
import com.ecci.parcial2.adapter.StudentProtocol
import com.ecci.parcial2.dialogs.StudentDialog
import com.ecci.parcial2.model.Student
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), StudentProtocol {


    //Lateinit me permite asegurarle a
    // Android que esta variable sera inicializada más adelante
    private lateinit var adapter: StudentAdapter
    var studentToRemove: MutableList<Student> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()
        nextActivity()
    }

    private fun nextActivity(){

        nextButton.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            return@setOnClickListener
         }
    }

    private fun setupButtons() {
        addProductButton.setOnClickListener {
            val dialog = StudentDialog(this, "", "", "","", isModifier = false) { name, lastName, doc, phone->
                addStudent(name, lastName, doc, phone)
            }
            dialog.show()
        }
        removeProductsButton.setOnClickListener {
            if (studentToRemove.isEmpty()){
                Toast.makeText(this, "Seleccione los productos que desea eliminar", Toast.LENGTH_SHORT).show()
            }
            else{
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Confirmación")
                builder.setMessage("¿Esta seguro que quiere eliminar los productos seleccionados?")
                builder.setPositiveButton("Si"){_,_ ->
                    var removedStudents: MutableList<Student> = mutableListOf(Student("","","","", isSelected = false))
                    for (product in studentToRemove){
                        deleteStudent(product)
                        removedStudents.add(product)
                    }
                    for (student in removedStudents){
                        studentToRemove.remove(student)
                    }
                }
                builder.setNegativeButton("Cancelar"){_,_ ->
                }
                builder.show()

            }
        }
    }

    private fun setupList() {
            val students : MutableList<Student> = mutableListOf()
            adapter = StudentAdapter(students, this) { item, isDelete ->
            if(isDelete) deleteStudent(item)
            else editStudent(item)
        }
        studentRecyclerView.adapter = adapter
        studentRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addStudent(name: String, lastName: String, doc: String, phone: String) {
        val student = Student(name, lastName, doc, phone, isSelected = false)
        adapter.addStudent(student)
        adapter.notifyDataSetChanged()
    }

    private fun deleteStudent(student: Student) {
        //Crear una confirmación
        adapter.deleteProduct(student)
        adapter.notifyDataSetChanged()


    }

    fun updateStudent(student: Student, name: String, lastName: String, doc: String, phone: String) {

        adapter.editStudent(student, name, lastName, doc, phone)
        adapter.notifyDataSetChanged()
    }

    private fun editStudent(student: Student) {
        val dialog = StudentDialog(this, student.name, student.lastName, student.doc, student.phone, isModifier = true) { name, lastName, doc, phone->
            updateStudent(student, name, lastName, doc,phone)
        }
        dialog.show()
    }

    override fun didProductSelect(student: Student) {
        adapter.notifyDataSetChanged()
    }

    override fun productsToDelete(student: Student, isToDelete: Boolean) {
        if(isToDelete){
            studentToRemove.add(student)
        }else{
            studentToRemove.remove(student)
        }
    }

    override fun hideActivtyText(students: MutableList<Student>) {
        if (students.isEmpty()){
            activityText.visibility = View.VISIBLE
        }else{
            activityText.visibility = View.GONE
        }
    }

}